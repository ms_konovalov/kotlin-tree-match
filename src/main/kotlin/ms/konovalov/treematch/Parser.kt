package ms.konovalov.treematch

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.ArrayNode
import java.io.IOException
import java.io.InputStream
import java.util.*

// Storage JSON model
/**
 * Whole quiz
 */
data class Quiz(
        @JsonDeserialize(using = QuestionsMapDeserializer::class)
        @JsonProperty("questions") val questions: Map<Int, Question>,
        @JsonDeserialize(using = StepsMapDeserializer::class)
        @JsonProperty("steps") val steps: Map<Int, Step>,
        @JsonDeserialize(using = ResultsMapDeserializer::class)
        @JsonProperty("results") val results: Map<Int, Result>
)

/**
 * Union type for what is referred by step: question or result
 */
interface QuestionOrResult

/**
 * Question
 * We ignore `validation` field as it seems to be ambiguous
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class Question(
        @JsonProperty("id") val id: Int,
        @JsonProperty("question") val text: String
) : QuestionOrResult

/**
 * Step of the quiz which may be AnswerStep or ResultStep
 */
interface Step {
    val id: Int
}

/**
 * Step that refers to next question
 */
data class AnswerStep(
        @JsonProperty("id") override val id: Int,
        @JsonProperty("question_id") val questionId: Int,
        @JsonProperty("answers") val answers: Map<String, Int>
) : Step

/**
 * Step that refers to matching result
 */
data class ResultStep(
        @JsonProperty("id") override val id: Int,
        @JsonProperty("result_id") val resultId: Int
) : Step

/**
 * Matching result of the quiz
 */
data class Result(
        @JsonProperty("id") val id: Int,
        @JsonProperty("name") val name: String,
        @JsonProperty("description") val description: String
) : QuestionOrResult

/**
 * Deserializer that supports inheritance for [Step] and its children
 * It determines the instance of the created object by checking the fields in json object
 */
class InheritanceDeserializer : StdDeserializer<Step>(Step::class.java) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Step {
        val tree = p.readValueAsTree<JsonNode>()
        if (tree.has("id") && tree.has("result_id")) {
            return p.codec.treeToValue(tree, ResultStep::class.java)
        } else if (tree.has("id") && tree.has("question_id") && tree.has("answers")) {
            return p.codec.treeToValue(tree, AnswerStep::class.java)
        }
        throw IllegalStateException("Unable to deserialize node $tree")
    }
}

/**
 * We assign deserializer through [DeserializerModifier] as we need it only for parent class [Step] and not for children
 */
internal class DeserializerModifier : BeanDeserializerModifier() {
    override fun modifyDeserializer(config: DeserializationConfig?, beanDesc: BeanDescription?, deserializer: JsonDeserializer<*>?): JsonDeserializer<*> {
        val jsonDeserializer = super.modifyDeserializer(config, beanDesc, deserializer)
        val clazz = jsonDeserializer.handledType()
        if (Step::class.java == clazz) {
            return InheritanceDeserializer()
        }
        return jsonDeserializer
    }
}

object QuestionsMapDeserializer : StringHashMapValueDeserializer<Int, Question>(Question::class.java, Question::id)

object ResultsMapDeserializer : StringHashMapValueDeserializer<Int, Result>(Result::class.java, Result::id)

object StepsMapDeserializer : StringHashMapValueDeserializer<Int, Step>(Step::class.java, Step::id)

/**
 * Deserializer that parses list into hash map where the key value is determined by lambda function provided
 * Also check for uniqueness of the items: throws [RuntimeException] if there are several items with the same key
 * @param clazz class of the deserialized entity
 * @param extract lambda to calculate key
 * @throws RuntimeException if there are several items with the same calculated key value
 */
open class StringHashMapValueDeserializer<I, T>(private val clazz: Class<T>, private val extract: (T) -> I) : JsonDeserializer<HashMap<I, T>>() {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): HashMap<I, T> {
        val result = HashMap<I, T>()
        val arrayNode: TreeNode = p.codec.readTree(p)
        if (arrayNode.isArray && arrayNode is ArrayNode) {
            arrayNode.forEach {
                val value = p.codec.treeToValue(it, clazz)
                val key = extract.invoke(value)
                if (result.containsKey(key)) {
                    throw RuntimeException("Duplicate entity with id=$key")
                }
                result[key] = value
            }
        }
        return result
    }
}

/**
 * Parser for JSON document
 */
object QuizParser {

    private val objectMapper = ObjectMapper()

    init {
        val simpleModule = SimpleModule()
        simpleModule.setDeserializerModifier(DeserializerModifier())
        objectMapper.registerModule(simpleModule)
    }

    fun parse(input: InputStream): Quiz = objectMapper.readValue<Quiz>(input, Quiz::class.java)
}
