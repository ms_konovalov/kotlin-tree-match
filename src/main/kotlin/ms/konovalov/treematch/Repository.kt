package ms.konovalov.treematch

import org.springframework.stereotype.Component
import java.io.InputStream
import java.util.*

/**
 * Repository that takes data from predefined json file and provides methods to access data
 */
class Repository(private val inputStream: InputStream) {
    private val data: Quiz = QuizParser.parse(inputStream)

    init {
        validate()
    }

    /**
     * Get step in the quiz by id
     * @param id - integer identifier
     * @return step in the quiz
     */
    fun getStep(id: Int) = data.steps[id]

    /**
     * Get initial step for the Quiz or throw an exception
     * @throws RepositoryException if initial step is not defined
     * @return initial step for the Quiz
     */
    fun getInitStep() = getStep(1) ?: throw RepositoryException("Unable to find initial question")

    /**
     * Get question by id
     * @param id - integer identifier
     * @return question of the quiz
     */
    fun getQuestion(id: Int) = data.questions[id]

    /**
     * Get question by id
     * @param id - integer identifier
     * @return result of the quiz
     */
    fun getResult(id: Int) = data.results[id]

    /**
     * Get question or result for the specified step. Throw an error if data is inconsistent
     * @param step step of the quiz. If step is ResultStep then result will be returned
     * @throws RepositoryException if question or result is not found or if Step instance of not expected type
     */
    fun getQuestionOrResult(step: Step): QuestionOrResult =
            when (step) {
                is AnswerStep -> getQuestion(step.questionId)
                        ?: throw RepositoryException("Unable to find question with id=${step.questionId}")
                is ResultStep -> getResult(step.resultId)
                        ?: throw RepositoryException("Unable to find result with id=${step.resultId}")
                else -> throw RepositoryException("Something went wrong")
            }

    /**
     * Validate the data of the quiz
     * - check that there is at least 1 question, 1 result and 1 step
     * - check that for each step the referred question exists
     * - check that for each result step the referred result exists
     * - check that each branch is complete and ends with result
     * - detect loops in quiz branches
     */
    private fun validate() {
        if (data.questions.isEmpty()) {
            throw RuntimeException("There is no questions in this quiz")
        }
        if (data.steps.isEmpty()) {
            throw RuntimeException("There is no steps in this quiz")
        }
        if (data.results.isEmpty()) {
            throw RuntimeException("There is no steps in this quiz")
        }
        // check that no questions are missing
        data.steps.values.filterIsInstance<AnswerStep>().forEach {
            if (!data.questions.contains(it.questionId)) {
                throw RuntimeException("The question with id=${it.questionId} is missing")
            }
        }
        // check that no results are missing
        data.steps.values.filterIsInstance<ResultStep>().forEach {
            if (!data.results.contains(it.resultId)) {
                throw RuntimeException("The result with id=${it.resultId} is missing")
            }
        }
        // check that each branch ends with the result
        checkBranches()
    }

    private fun checkBranches() {
        val visited = HashSet<Int>()
        val step = getInitStep()
        checkBranch(step, visited)
    }

    /**
     * Check branch recursively to detect cycles
     */
    private fun checkBranch(step: Step, visited: Set<Int>) {
        if (visited.contains(step.id)) {
            throw RepositoryException("There is a cycle in the quiz with step id=${step.id}")
        }
        when (getQuestionOrResult(step)) {
            is Result -> return // do nothing
            is Question -> (step as AnswerStep).answers.forEach { (_, nextStepId) ->
                val nextStep = getStep(nextStepId)
                        ?: throw RepositoryException("Unable to find step with id=${step.id}")
                checkBranch(nextStep, visited.plusElement(step.id))
            }
        }
    }
}

/**
 * Exception that happens if data is inconsistent
 */
class RepositoryException(override val message: String) : RuntimeException(message)
