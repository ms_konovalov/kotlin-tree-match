package ms.konovalov.treematch

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

// API Model

interface Response

data class QuestionDTO(
        @JsonProperty("step_id") val stepId: Int,
        @JsonProperty("question") val text: String,
        @JsonProperty("answers") val answers: Set<String>) : Response

data class AnswerDTO(
        @JsonProperty("step_id") val stepId: Int,
        @JsonProperty("answer") val text: String
)

data class MatchDTO(
        @JsonProperty("name") val name: String,
        @JsonProperty("description") val description: String
) : Response

/**
 * Controller class for API methods
 */
@RestController
@RequestMapping("api")
class Controller(@Value("classpath:questions.json") private val resource: Resource) {

    val repository = Repository(resource.inputStream)

    /**
     * Get initial question
     */
    @GetMapping("/begin", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun begin(): Response {
        val step = repository.getInitStep()
        return getQuestionOrMatch(step)
    }

    /**
     * Post answer to the question and get the next question or matching result
     */
    @PostMapping("/answer", consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun answer(@RequestBody answer: AnswerDTO): Response {
        val nextStep = when (val step = repository.getStep(answer.stepId)
                ?: throw BadRequestException("Unable to find step with id=${answer.stepId}")) {
            is AnswerStep -> {
                val nextStepId = step.answers[answer.text]
                        ?: throw BadRequestException("Wrong answer to step with id=${answer.stepId}. " +
                                "Possible answers are ${step.answers.keys.joinToString(prefix = "[", postfix = "]")}")
                repository.getStep(nextStepId)
                        ?: throw InternalErrorException("Unable to find step with id=${nextStepId}")
            }
            is ResultStep ->
                throw BadRequestException("Unable to find step with id=${answer.stepId}")
            else -> throw InternalErrorException("Something went wrong")
        }
        return getQuestionOrMatch(nextStep)
    }

    private fun getQuestionOrMatch(step: Step): Response {
        return when (val qr = repository.getQuestionOrResult(step)) {
            is Question -> QuestionDTO(step.id, qr.text, (step as AnswerStep).answers.keys)
            is Result -> MatchDTO(qr.name, qr.description)
            else -> throw InternalErrorException("Something went wrong")
        }
    }

    @ExceptionHandler(RepositoryException::class)
    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    fun handle(e: RepositoryException) {
        e.printStackTrace()
    }

    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    class InternalErrorException(override val message: String) : RuntimeException()

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    class BadRequestException(override val message: String) : RuntimeException()
}


@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
