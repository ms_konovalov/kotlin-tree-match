package ms.konovalov.treematch

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc

import org.hamcrest.Matchers.containsString
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class TestingWebApplicationTest(@Autowired private val mockMvc: MockMvc) {

    @Test
    fun `should return initial question`() {
        mockMvc.perform(get("/api/begin")).andDo(print()).andExpect(status().isOk)
                .andExpect(content().string(containsString(""""step_id":1""")))
    }

    @Test
    fun `should return next question`() {
        mockMvc.perform(post("/api/answer").contentType(MediaType.APPLICATION_JSON)
                .content("""{"step_id":1, "answer": "farm"}"""))
                .andDo(print()).andExpect(status().isOk).andExpect(content().string(containsString("""Do you want a fruiting tree?""")))
    }

    @Test
    fun `should fail for wrong step number`() {
        mockMvc.perform(post("/api/answer").contentType(MediaType.APPLICATION_JSON)
                .content("""{"step_id":13, "answer": "farm"}"""))
                .andDo(print()).andExpect(status().isBadRequest)
    }

    @Test
    fun `should fail for wrong answer`() {
        mockMvc.perform(post("/api/answer").contentType(MediaType.APPLICATION_JSON)
                .content("""{"step_id":1, "answer": "yay"}"""))
                .andDo(print()).andExpect(status().isBadRequest)
    }

    @Test
    fun `should return match`() {
        mockMvc.perform(post("/api/answer").contentType(MediaType.APPLICATION_JSON)
                .content("""{"step_id":6, "answer": 10}"""))
                .andDo(print()).andExpect(status().isOk).andExpect(content().string(containsString(""""name":"Lemon tree"""")))
    }
}
