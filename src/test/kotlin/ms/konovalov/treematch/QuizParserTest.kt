package ms.konovalov.treematch

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.exc.ValueInstantiationException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class QuizParserTest {

    @Test
    fun `test with big quiz file`() {
        val input = QuizParserTest::class.java.getResource("/questions.json").openStream()
        val result = QuizParser.parse(input)
        Assertions.assertEquals(9, result.questions.size)
        Assertions.assertEquals(28, result.steps.size)
        Assertions.assertEquals(16, result.results.size)
    }

    @Test
    fun `should fail on empty file`() {
        val empty = "{}"
        Assertions.assertThrows(ValueInstantiationException::class.java) {
            QuizParser.parse(empty.byteInputStream())
        }
        val empty2 = "{'questions': [], 'steps': []}"
        Assertions.assertThrows(JsonParseException::class.java) {
            QuizParser.parse(empty2.byteInputStream())
        }
    }

    @Test
    fun `should fail on duplicate question`() {
        val data = """{
            |"questions": [{ "id": 2, "question": "Do you like cooking?", "validation": ["yes", "no"] },
            |              { "id": 2, "question": "Do you like cooking?", "validation": ["yes", "no"] }], 
            |"steps":     [{"id": 1, "question_id": 1, "answers": { "courtyard": 2, "garden": 5, "farm": 8 } }],
            |"results":   [{ "id": 1, "name": "Curry leaf tree", "description": "A small tree that's great" }]
            |}""".trimMargin()
        Assertions.assertThrows(JsonMappingException::class.java) {
            QuizParser.parse(data.byteInputStream())
        }
    }


    @Test
    fun `should fail on missing question`() {
        val data = """{
            |"questions": [{ "id": 2, "question": "Do you like cooking?", "validation": ["yes", "no"] }], 
            |"steps": [{"id": 1, "question_id": 1, "answers": { "courtyard": 2, "garden": 5, "farm": 8 } }],
            |"results": [{ "id": 1, "name": "Curry leaf tree", "description": "A small tree that's great" }]
            |}""".trimMargin()
        Assertions.assertThrows(RuntimeException::class.java) {
            Repository(data.byteInputStream())
        }
    }
}
